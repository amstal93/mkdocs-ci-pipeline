# Mkdocs - Continuous Integration Pipeline

## Motivation

> The point is that for development of MkDocs, there is no need for a docker
> image...Regardless, I have given this some more thought and the MkDocs project
> itself has no use for a docker file. Adding one would only be more maintenance
> with no benefit for us. There is no reason why a third party can't maintain
> a docker file. In fact, we don't maintain any other system specific packages,
> so why would docker be any different? --[Waylan Limberg][upstream-docker-ref]

## Usage

```yml
include:
  - remote: 'https://gitlab.com/lramage/mkdocs-ci-pipeline/-/raw/v0.1.1/.gitlab-ci-template.yml'
```

## License

SPDX-License-Identifier: [MIT](https://spdx.org/licenses/MIT)

[upstream-docker-ref]: https://web.archive.org/web/20200519025526/https://github.com/mkdocs/mkdocs/pull/2015
